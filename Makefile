latexcmd = latexmk -pdf
pumlcmd = plantuml -tlatex:nopreamble
sources = $(wildcard Sources/*.tex)
diagrams = $(wildcard Diagrams/*.puml)
diagram_sources = $(diagrams:.puml=.latex)

all: Thesis.pdf Thesis_print.pdf Slides.pdf

Thesis.pdf: Thesis.tex $(sources) $(diagram_sources) Thesis.bib
	$(latexcmd) -jobname=Thesis $< </dev/null

Thesis_print.pdf: Thesis_print.tex $(sources) $(diagram_sources) Thesis.bib
	$(latexcmd) -jobname=Thesis_print $< </dev/null

Slides.pdf: Slides.tex $(diagram_sources) Thesis.bib
	$(latexcmd) -jobname=Slides $< </dev/null

diagrams: $(diagram_sources)

Diagrams/%.latex: Diagrams/%.puml
	$(pumlcmd) $<

pvc: Thesis.tex $(sources) $(diagram_sources) Thesis.bib
	-$(latexcmd) -jobname=Thesis -pvc $< </dev/null

pvc_slides: Slides.tex $(diagram_sources) Thesis.bib
	-$(latexcmd) -jobname=Slides -pvc $< </dev/null

count: Thesis.tex $(sources) Thesis.bib
	@letters=`texcount -nosub -sum -letter -merge -q -1 $<`; \
	words=`texcount -nosub -sum -merge -q -1 $<`; \
	chars=`echo "$$words + $$letters" | bc -l`; \
	pages=`echo "$$chars / 1800" | bc -l`; \
	pages_left_min=`echo "20 - $$pages" | bc -l`; \
	pages_left_rec=`echo "30 - $$pages" | bc -l`; \
	echo "$$letters letters"; \
	echo "$$words words (≈ spaces)"; \
	echo "$$chars chars"; \
	echo "$$pages pages"; \
	if [ `echo "$$pages_left_min > 0" | bc -l` = 1 ]; then \
		echo "$$pages_left_min pages left to minimum count"; \
	else \
		echo "$$pages_left_rec pages left to recommended count"; \
	fi

clean:
	latexmk -C
	$(RM) -f Diagrams/*.latex

.PHONY: all diagrams pvc count clean
