\chapter{Existujúce otvorené systémy SCADA}\label{chp:scada_systems}

V tejto kapitole popisujem existujúce otvorené systémy SCADA, ich znaky a~výhody
respektíve nevýhody daných systémov.

\section{Scada-LTS}

Scada-LTS\footnote{\url{https://github.com/SCADA-LTS/Scada-LTS/}} je samostatnou
vývojovou vetvou systému SCADA
ScadaBR\footnote{\url{https://github.com/ScadaBR/ScadaBR}}.\;\cite{scada-lts_team}
Funguje plne vo~webovom rozhraní, či už ide o~jeho nastavenie, alebo následné
používanie.
Naprogramovaný je v~jazyku Java s~využitím frameworku Spring.

Systém je stále v~aktívnom vývoji, časť grafického rozhrania je prerobená ako
jednostránková aplikácia vo~frameworku Vue.js, ale väčšina častí používa ešte
staré grafické rozhranie v~technológii JSP (\texten{Jakarta Server Pages},
predtým \texten{Java Server Pages}).
Aplikácia však obsahuje aj niekoľko chýb, na~stránke projektu na~GitHube je ku
dňu 16.\;11.\;2022 okolo 235 hlásení, z~toho 58 s~označením
\texten{bug}.\;\cite{scada-lts_issues}
Jednu ešte neohlásenú chybu programu som objavil aj pri~úvodnom testovaní
použiteľnosti, klávesová skratka na~vypnutie režimu zobrazenia na~celú obrazovku
v~časti \texten{Graphical Views} nefunguje.
Dočasné riešenie je vymazanie súboru \texten{cookie}, v~ktorom je toto
nastavenie uložené, a~obnovenie stránky.

Scada-LTS podporuje veľké množstvo protokolov na~zbieranie dát od~iných
systémov, medzi nimi sú aj protokoly Modbus, SNMP, BACnet
(Obr.~\ref{fig:scada-lts_bacnet}) a~HTTP (či už aktívne žiadanie dát, alebo len
pasívne prijímanie).

\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/scada-lts_bacnet.png}
    \caption{Nastavenie zdroja dát BACnet v~systéme Scada-LTS}%
    \label{fig:scada-lts_bacnet}
\end{figure}

Možnosti nastavenia zdrojov dát HTTP nie sú ideálne.
V~prípade, že požiadavku na dáta robí Scada-LTS, tak jedinou možnosťou, ako
extrahovať konkrétne namerané hodnoty z~HTTP odpovede je regulárny výraz.
Prácu so~štruktúrovanými dokumentmi, ako sú napríklad JSON (\texten{JavaScript
Object Notation}) alebo XML (\texten{Extensible Markup Language}), a~následné
nájdenie hodnoty pomocou technológií
JSONPath\footnote{\url{https://tools.ietf.org/id/draft-goessner-dispatch-jsonpath-00.html}}
respektíve
XPath\footnote{\url{https://developer.mozilla.org/en-US/docs/Web/XPath}}
nepodporuje.

Tak isto aj spôsob, kedy Scada-LTS čaká na~dáta cez HTTP, má svoje limity.
Na~všetky zdroje dát čaká na~jednej URL (\texten{Uniform Resource Locator}) a~nie
je možné ju zmeniť tak, aby napríklad každé zariadenie malo svoju vlastnú.
Jediným rozlíšením medzi jednotlivými zdrojmi je názov \texten{Data Point}.
Taktiež v~tomto prípade nie je možné posielať štruktúrované dokumenty, jedinými
dvomi možnosťami sú parametre požiadavky HTTP GET a~telo s~typom
\verb|application/x-www-form-urlencoded| v~požiadavke HTTP POST.
Zároveň pri protokole HTTP nie je možné hodnotu nastaviť a~poslať naspäť.

Zaznamenané hodnoty je možné sledovať pomocou zoznamov \texten{Watch List},
ktoré sa dajú pomenovať a~priradiť do nich rôzne sledované premenné
(Obr.~\ref{fig:scada-lts_watchlist}).
Pri každej premennej sa zobrazuje aj čas poslednej hodnoty a~upozornenia
na~alarmy, ktoré ešte neboli vyriešené.
Pod~zoznamom je vykreslený aj interaktívny graf (Obr.~\ref{fig:scada-lts_graph})
historických hodnôt premenných zaradených v~aktuálnom zozname.

\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{./Images/StateOfTheArt/scada-lts_watchlist_w_alarm.png}
    \caption{Zoznam sledovaných premenných v~systéme Scada-LTS}%
    \label{fig:scada-lts_watchlist}
\end{figure}
\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{./Images/StateOfTheArt/scada-lts_datapoint_graph_small_w_highlight.png}
    \caption{Interaktívny graf historických hodnôt sledovaných premenných
    v~systéme Scada-LTS}%
    \label{fig:scada-lts_graph}
\end{figure}

Scada-LTS umožňuje aj tvorbu vlastných ovládacích panelov \texten{Graphical
View} (Obr.~\ref{fig:scada-lts_view} a~\ref{fig:scada-lts_view_edit}).
Tie môžu mať vlastné pozadie vo forme obrázku a~obsahovať niekoľko statických
alebo dynamických komponentov.
Príkladmi takýchto komponentov sú:
\begin{itemize}
    \item grafy hodnôt sledovaných premenných,
    \item textové bloky s~hodnotami sledovaných premenných,
    \item vlastné HTML vrátane JavaScriptu bežiaceho v~prehliadači,
    \item skript generujúci obsah na serveri.
\end{itemize}

\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/scada-lts_views-mech.png}
    \caption{\texten{Graphical View} v~systéme Scada-LTS}%
    \label{fig:scada-lts_view}
\end{figure}
\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/scada-lts_views_edit.png}
    \caption{Úprava \texten{Graphical View} v~systéme Scada-LTS}%
    \label{fig:scada-lts_view_edit}
\end{figure}

Scada-LTS samozrejme obsahuje aj ďalšie funkcie, ako sú napríklad alarmy
s~posielaním oznámení e-mailom.
Na~odoslanie oznámenia cez SMS Scada-LTS potrebuje externý program, ktorý
prevedie e-mail na SMS a~doručí ju.
Scada-LTS taktiež podporuje správu viacerých používateľov s~kontrolou prístupu
k~jednotlivým častiam systému a~export zaznamenaných hodnôt a~udalostí.

\section{OpenSCADA}

OpenSCADA\footnote{OpenSCADA web: \url{http://oscada.org}} je ďalším otvoreným
systémom SCADA.
Tento systém funguje ako desktopová aplikácia, avšak konfigurácia a~interakcia
s~vytvorenými ovládacími panelmi je možná aj cez webové rozhranie.
Taktiež podporuje zapnutie bez desktopového grafického rozhrania ako démon,
čo je vhodné pre~produkčný beh na~serveri.
Úprava používateľských ovládacích panelov je však možná len pomocou desktopového
grafického rozhrania, nie cez web.

Systém OpenSCADA je naprogramovaný v~jazykoch C/C++ s~využitím grafického
frameworku Qt.
Jeho jednotlivé časti sú rozdelené do~subsystémov
a~modulov.\;\cite{openscada_manual}
Tento systém je tiež stále vyvíjaný, ale na~rozdiel od~Scada-LTS pôsobí
robustnejšie a~počas jeho testovania som nenarazil na~žiadnu očividnú programovú
chybu.

Protokolov na~získavanie dát je dostupných menej ako v~Scada-LTS.
Síce podporuje protokoly Modbus a~SNMP, iné, ako napríklad HTTP alebo BACnet,
chýbajú.\;\cite{openscada_daq}

Zobrazenie hodnôt sledovaných premenných je pomocou modulu \texten{Archives}
možné aj bez vytvorenia vlastného ovládacieho panelu, ale dajú sa zobraziť iba
samostatne v~statickej tabuľke (Obr.~\ref{fig:openscada_ar_table}) alebo
statickom grafe (Obr.~\ref{fig:openscada_ar_graph}).

\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/openscada_archive_table.png}
    \caption{Tabuľka hodnôt v~systéme OpenSCADA}%
    \label{fig:openscada_ar_table}
\end{figure}
\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/openscada_archive_graph.png}
    \caption{Graf hodnôt v~systéme OpenSCADA}%
    \label{fig:openscada_ar_graph}
\end{figure}

Systém taktiež podporuje správu viacerých používateľov a~skupín, ale kontrola
prístupu je len na~úrovni subsystémov pomocou preddefinovaných skupín.
Napríklad, aby používateľ vedel zobraziť historické hodnoty sledovaných
premenných, musí byť v~skupine \texten{Archive}.
To mu umožní nielen tabuľku hodnôt a~graf zobraziť, ale aj meniť nastavenia
archivácie všetkých sledovaných premenných.

OpenSCADA taktiež umožňuje zasielanie SMS a~e-mailov pomocou subsystému
\texten{Transports} (Obr.~\ref{fig:openscada_nofifs}).
Na~posielanie SMS využíva priame pripojenie na~hardvér sériovou linkou.
Systém však neumožňuje jednoduché nastavenie spúšťacieho mechanizmu alarmu,
napríklad po~prekročení stanovenej hodnoty sledovanej premennej.
Implementácia takýchto alarmov musí byť pridaná ručne skriptom v~module
\texten{JavaLikeCalc} subsystému \texten{Data Acquisition}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{./Images/StateOfTheArt/openscada_notifs.png}
    \caption{Nastavenia zasielania SMS a~e-mailov v~systéme OpenSCADA}%
    \label{fig:openscada_nofifs}
\end{figure}

V neposlednom rade poskytuje OpenSCADA aj nástroj na~tvorbu používateľských
ovládacích panelov (Obr.~\ref{fig:openscada_ui_cfg}).
Tento nástroj však nie je možné spustiť vo~webovom prehliadači.
Jeho možnosti sú naozaj rozsiahle a~umožňuje vytvorenie pokročilých rozhraní
obsahujúcich rôzne schémy, grafy a~kontrolné prvky
(Obr.~\ref{fig:openscada_ui_aglks}), ale nie je veľmi používateľsky prívetivý.
Aby sa rozhranie zobrazilo, jednotlivé jeho časti musia byť v~správnej
hierarchii a~so~správne pomenovaným identifikátorom, napríklad pre~hlavnú
stránku to je názov ,,\verb|so|``.

\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{./Images/StateOfTheArt/openscada_ui_cfg.png}
    \caption{Nástroj na~tvorbu ovládacích panelov v~systéme OpenSCADA}%
    \label{fig:openscada_ui_cfg}
\end{figure}
\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/openscada_ui_aglks.png}
    \caption{Ovládací panel demo projektu systému OpenSCADA}%
    \label{fig:openscada_ui_aglks}
\end{figure}
\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{./Images/StateOfTheArt/openscada_ui_if.png}
    \caption{Jednoduchý ovládací panel so~statickým grafom v~systéme OpenSCADA}%
    \label{fig:openscada_ui_if}
\end{figure}
